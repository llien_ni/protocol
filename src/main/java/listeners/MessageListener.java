package listeners;


import protocol.MessagePacket;
import server.Server;
import server.SocketClient;

import java.net.Socket;


public interface MessageListener {

    void init(Server server);

    void handleMessage(MessagePacket messagePacket, SocketClient socketClient);

    byte getTypeOFMessage();
    byte getSubTypeOfMessage();

}
