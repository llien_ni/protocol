package listeners;

import protocol.MessagePacket;
import protocol.SubTypeOfMessage;
import protocol.TypeOfMessage;
import server.SocketClient;

public class StandardNotSecureTypeListener extends AbstractMessageListener{


    public StandardNotSecureTypeListener() {
        super(TypeOfMessage.STANDARD, SubTypeOfMessage.STANDARD_NOT_SECURED);
    }

    @Override
    public void handleMessage(MessagePacket messagePacket, SocketClient socketClient) {
        String text = messagePacket.getContentFromField(0, String.class);


        MessagePacket answer = MessagePacket.create(getTypeOFMessage(), getSubTypeOfMessage());
        answer.setContentInField(0, text);


        server.sendMessage(answer, socketClient);
    }

    @Override
    public byte getTypeOFMessage() {
        return TypeOfMessage.STANDARD.getType();
    }

    @Override
    public byte getSubTypeOfMessage() {
        return SubTypeOfMessage.STANDARD_NOT_SECURED.getSubType();
    }
}
