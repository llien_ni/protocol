package listeners;

import protocol.MessagePacket;
import protocol.SubTypeOfMessage;
import protocol.TypeOfMessage;
import server.SocketClient;

import java.io.IOException;
import java.net.Socket;

public class HandShakeTypeListener extends AbstractMessageListener{


    public HandShakeTypeListener() {
        super(TypeOfMessage.META, SubTypeOfMessage.HANDSHAKE);
    }

    @Override
    public void handleMessage(MessagePacket messagePacket, SocketClient socketClient) {

        String pong ="pong";
        MessagePacket answer = MessagePacket.create(getTypeOFMessage(), getSubTypeOfMessage());
        answer.setContentInField(0, pong);

        server.sendMessage(answer, socketClient);
    }

    @Override
    public byte getTypeOFMessage() {
        return TypeOfMessage.META.getType();
    }

    @Override
    public byte getSubTypeOfMessage() {
        return SubTypeOfMessage.HANDSHAKE.getSubType();
    }
}
