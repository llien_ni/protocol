package listeners;


import protocol.SubTypeOfMessage;
import protocol.TypeOfMessage;
import server.Server;

public abstract class AbstractMessageListener implements MessageListener {

    protected Server server;
    protected TypeOfMessage type;
    protected SubTypeOfMessage subType;

    public AbstractMessageListener(TypeOfMessage type, SubTypeOfMessage subType) {

        this.type = type;
        this.subType = subType;
    }

    @Override
    public void init(Server server) {
        this.server = server;
    }
}
