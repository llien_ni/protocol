package listeners;

import protocol.MessagePacket;
import protocol.SubTypeOfMessage;
import protocol.TypeOfMessage;
import server.SocketClient;

public class GoodByeTypeListener extends AbstractMessageListener{


    public GoodByeTypeListener() {
        super(TypeOfMessage.META, SubTypeOfMessage.GOODBYE);
    }

    @Override
    public void handleMessage(MessagePacket messagePacket, SocketClient socketClient) {

        MessagePacket answer = MessagePacket.create(getTypeOFMessage(), getSubTypeOfMessage());
        answer.setContentInField(0, "end");


        server.sendMessage(answer, socketClient);
    }

    @Override
    public byte getTypeOFMessage() {
        return TypeOfMessage.META.getType();
    }

    @Override
    public byte getSubTypeOfMessage() {
        return SubTypeOfMessage.GOODBYE.getSubType();
    }
}