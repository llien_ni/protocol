package client;


import protocol.MessagePacket;
import protocol.TypeOfMessage;

import java.io.IOException;
import java.io.InputStream;

public class ClientThread implements Runnable {
    private Client client;

    private boolean isWorking = true;


    public ClientThread(Client client) {

        this.client = client;

    }

    @Override
    public void run() {

        try {
            while (isWorking) {

                MessagePacket message = MessagePacket.parse(readInput(client.getSocket().getInputStream()));
                byte type = message.getType();
                byte subType = message.getSubType();

                System.out.println("получили сообщение от сервера с типом: " + message.getType() + " " + TypeOfMessage.findTypeBy(type, subType));

                String contentFromField = message.getContentFromField(0, String.class);

                System.out.println("SGet message");
                System.out.println(contentFromField);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static byte[] extendArray(byte[] oldArray) {
        int oldSize = oldArray.length;
        byte[] newArray = new byte[oldSize * 2];
        System.arraycopy(oldArray, 0, newArray, 0, oldSize);
        return newArray;
    }

    private static byte[] readInput(InputStream stream) throws IOException {
        int b;
        byte[] buffer = new byte[10];
        int counter = 0;
        while ((b = stream.read()) > -1) {
            buffer[counter++] = (byte) b;
            if (counter >= buffer.length) {
                buffer = extendArray(buffer);
            }
            if (counter > 1 && MessagePacket.compareEndOfPacket(buffer, counter - 1)) {
                break;
            }
        }
        byte[] data = new byte[counter];
        System.arraycopy(buffer, 0, data, 0, counter);
        return data;
    }


}
