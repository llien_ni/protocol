package client;




import lombok.Data;
import lombok.ToString;
import protocol.MessagePacket;

import java.io.IOException;
import java.net.Socket;

@Data
@ToString
public class Client {

    private Socket socket;
    private ClientThread clientThread;


    public boolean sendMessage(MessagePacket messagePacket) {
        boolean isSuccessful;

        try {
            socket.getOutputStream().write(messagePacket.toByteArrayMy());
            socket.getOutputStream().flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


        isSuccessful = true;

        return isSuccessful;
    }

    public void start() throws IOException {
        socket = new Socket("localhost", 4444);


        clientThread = new ClientThread(this);


        new Thread(clientThread).start();
    }



}
