package oldprotocol;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class StandardTypeHandler {


    private MainPacket packet;

    public StandardTypeHandler(MainPacket packet) {
        this.packet = packet;
    }

    public void setValue(int id, String value){
        MainPacket.MainField field;


        try{
            field = packet.getField(id);

        }catch(IllegalArgumentException e){
            field = new MainPacket.MainField((byte) id);
        }

        try(ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream)){

            objectOutputStream.writeObject(value);
            byte[] data = byteArrayOutputStream.toByteArray();

            if(data.length>255){
                throw  new IllegalArgumentException("Too mach data sent");
            }


            field.setSize((byte) data.length);
            field.setContent(data);

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        packet.getFields().add(field);
    }

}
