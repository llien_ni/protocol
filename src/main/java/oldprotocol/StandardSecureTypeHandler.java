package oldprotocol;

import java.io.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.Optional;
import javax.crypto.*;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import static oldprotocol.MainPacket.*;

public class StandardSecureTypeHandler {


    private MainPacket packet;

    private KeyPacket keyPacket;

    private byte[] key;
    private byte[] salt;


    public StandardSecureTypeHandler(MainPacket packet) {
        this.packet = packet;
    }

    public StandardSecureTypeHandler() {
    }

    public void addKey(KeyPacket packet){

        byte[] encodedPacket = packet.toByteArray();

        KeyPacket keyPacket = KeyPacket.parse(encodedPacket);

        this.keyPacket = keyPacket;
        this.key = keyPacket.getKey();

        this.salt = keyPacket.getSalt();



    }


    public static SecretKey getKeyFromPassword(String password, String salt)
            throws NoSuchAlgorithmException, InvalidKeySpecException {

        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt.getBytes(), 2, 256);
        SecretKey secret = new SecretKeySpec(factory.generateSecret(spec)
                .getEncoded(), "AES");
        return secret;
    }


    public void setValue(int id, String value){
        MainPacket.MainField field;


        try{
            field = packet.getField(id);

        }catch(IllegalArgumentException e){
            field = new MainPacket.MainField((byte) id);
        }

        try(ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream)){


            Cipher cipher = Cipher.getInstance("AES");

            String key = Base64.getEncoder().encodeToString(this.key);
            String salt = Base64.getEncoder().encodeToString(this.salt);

            SecretKey secretKey = getKeyFromPassword(key, salt);

            cipher.init(Cipher.ENCRYPT_MODE, secretKey);


            objectOutputStream.writeObject(value);

            byte[] data = cipher.doFinal(byteArrayOutputStream.toByteArray());


            if(data.length>255){
                throw  new IllegalArgumentException("Too mach data sent");
            }


            field.setSize((byte) data.length);
            field.setContent(data);

        } catch (IOException | NoSuchAlgorithmException e) {
            throw new IllegalArgumentException(e);
        } catch (InvalidKeyException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException | InvalidKeySpecException e) {
            e.printStackTrace();
        }

        packet.getFields().add(field);
    }

    public String getValue(int id) {
        MainPacket.MainField field = packet.getField(id);
        Cipher cipher = null;
        try {
            String key = Base64.getEncoder().encodeToString(this.key);
            String salt = Base64.getEncoder().encodeToString(this.salt);

            SecretKey secretKey = getKeyFromPassword(key, salt);

            cipher = Cipher.getInstance("AES");

            cipher.init(Cipher.DECRYPT_MODE, secretKey);

            byte[] plainText = cipher.doFinal(field.getContent());

            try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(plainText);
                 ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream)) {


                return (String) objectInputStream.readObject();

            } catch (IOException | ClassNotFoundException e) {
                throw new IllegalArgumentException(e);
            }


        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | InvalidKeySpecException e) {
            e.printStackTrace();
        }

        throw new IllegalArgumentException("Unknown packet format");

    }


    public byte[] decode(byte[] content){

        try {
            Cipher cipher = Cipher.getInstance("AES");
            String key = Base64.getEncoder().encodeToString(this.key);
            String salt = Base64.getEncoder().encodeToString(this.salt);

            SecretKey secretKey = getKeyFromPassword(key, salt);

            cipher.init(Cipher.DECRYPT_MODE, secretKey);

            System.out.println("Unknown packet format");

            byte[] plainText = cipher.doFinal(content);

            return plainText;
        } catch (NoSuchPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | BadPaddingException | InvalidKeyException e) {
            throw new IllegalArgumentException("Unknown packet format");
        } catch (InvalidKeySpecException e) {
            throw new IllegalArgumentException("Unknown packet format");
        }

    }

    public byte[] encode(byte[] content) {

        try {
            Cipher cipher = Cipher.getInstance("AES");



            String key = Base64.getEncoder().encodeToString(this.key);
            String salt = Base64.getEncoder().encodeToString(this.salt);

            SecretKey secretKey = getKeyFromPassword(key, salt);

            cipher.init(Cipher.ENCRYPT_MODE, secretKey);


            System.out.println(content);


            byte[] data = cipher.doFinal(content);
            return data;
        } catch (NoSuchPaddingException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException | NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }

        throw new IllegalArgumentException("Unknown packet format");

    }

    public byte[] toByteArray() {
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {

            outputStream.write(new byte[]{HEADER_1, HEADER_2});

            outputStream.write(packet.getType());
            outputStream.write(packet.getSubType());

            if (!packet.getFields().isEmpty()) {
                for (MainPacket.MainField field : packet.getFields()) {


                    Cipher cipher = Cipher.getInstance("AES");

                    String key = Base64.getEncoder().encodeToString(this.key);
                    String salt = Base64.getEncoder().encodeToString(this.salt);

                    SecretKey secretKey = getKeyFromPassword(key, salt);
                    //SecretKey secretKey = new SecretKeySpec(key, "AES");
                    cipher.init(Cipher.ENCRYPT_MODE, secretKey);


                    byte[] data = cipher.doFinal(field.getContent());


                    if(data.length>255){
                        throw  new IllegalArgumentException("Too mach data sent");
                    }


                    field.setSize((byte) data.length);
                    field.setContent(data);

                    outputStream.write(new byte[]{field.getId(), field.getSize()});

                    outputStream.write(data);




                }
            }
            outputStream.write(new byte[]{FOOTER_1, FOOTER_2});

            return outputStream.toByteArray();

        } catch (IOException | NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            throw new IllegalArgumentException(e);
        }
    }


    public MainPacket parse(byte[] data) {
        if (data[0] != HEADER_1 && data[1] != HEADER_2 || data[data.length - 1] != FOOTER_2 && data[data.length - 2] != FOOTER_1) {
            throw new IllegalArgumentException("Unknown packet format");

        }
        byte type = data[2];
        byte subType = data[3];
        MainPacket packet = MainPacket.create(type, subType);
        int offset = 4;
        this.packet = packet;
        while (true) {
            if (data.length - 2 <= offset) {
                return packet;
            }

            byte fieldId = data[offset];
            byte fieldSize = data[offset + 1];

            byte[] content = new byte[fieldSize];

            if (fieldSize != 0) {

                System.arraycopy(data, offset + 2, content, 0, fieldSize);

            }

            MainPacket.MainField field = new MainPacket.MainField(fieldId, fieldSize, decode(content));

            packet.getFields().add(field);

            offset += 2 + fieldSize;
        }


    }

    public MainPacket.MainField getField(int id) {
        Optional<MainPacket.MainField> field = packet.getFields().stream()
                .filter(f -> f.getId() == (byte) id)
                .findFirst();

        if (field.isEmpty()) {
            throw new IllegalArgumentException("No field with that id");
        }
        return field.get();
    }
}
