package oldprotocol;

import java.io.*;

public class GoodByeTypeHandler {


    private MainPacket packet;


    public GoodByeTypeHandler(MainPacket packet) {
        this.packet = packet;
    }

    public void finishConnection(){

        MainPacket.MainField field = new MainPacket.MainField((byte) 2);

        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream)) {

            objectOutputStream.writeObject("end");
            byte[] data = byteArrayOutputStream.toByteArray();

            field.setSize((byte) data.length);
            field.setContent(data);


        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        packet.getFields().add(field);

    }

}
