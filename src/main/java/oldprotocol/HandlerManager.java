package oldprotocol;

public class HandlerManager {


    public static <T> T handle(MainPacket packet){
        if(packet.getType() == 1 && packet.getSubType() == 1){
            HandShakeTypeHandler handler = new HandShakeTypeHandler(packet);
            return (T) handler;
        }
        if(packet.getType() == 1 && packet.getSubType() == 2){
            GoodByeTypeHandler handler = new GoodByeTypeHandler(packet);
            return (T) handler;
        }

        if(packet.getType() == 2 && packet.getSubType() == 1){
            StandardTypeHandler handler = new StandardTypeHandler(packet);
            return (T) handler;
        }
        if(packet.getType() == 2 && packet.getSubType() == 2){
            StandardSecureTypeHandler handler = new StandardSecureTypeHandler(packet);
            return (T) handler;
        }
        throw new IllegalArgumentException("could not define type");
    }

}
