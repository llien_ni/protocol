package oldprotocol;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Data
public class MainPacket {

    public static final byte HEADER_1 = (byte) 0xe3;
    public static final byte HEADER_2 = (byte) 0x14;


    public static final byte FOOTER_1 = (byte) 0x01;
    public static final byte FOOTER_2 = (byte) 0x70;

    private byte type;//2.1-standard 2.2-standard secure

    private byte subType; // 1.1-handshake 1.2-goodbye

    private List<MainField> fields;

    @Data
    @AllArgsConstructor
    public static class MainField {

        private byte id;
        private byte size;
        private byte[] content;

        public MainField(byte id) {
            this.id = id;

        }

    }

    public static MainPacket create(int type, int subType) {


        MainPacket packet = new MainPacket();


        packet.type = (byte) type;
        packet.subType = (byte) subType;
        packet.setFields(new ArrayList<>());
        return packet;

    }

    public static boolean compareEndOfPacket(byte[] arr, int lastItem) {
        return arr[lastItem - 1] == FOOTER_1 && arr[lastItem] == FOOTER_2;

    }

    public byte[] toByteArray() {
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {

            outputStream.write(new byte[]{HEADER_1, HEADER_2});

            outputStream.write(type);
            outputStream.write(subType);

            if (!fields.isEmpty()) {
                for (MainField field : fields) {
                    outputStream.write(new byte[]{field.getId(), field.getSize()});


                        outputStream.write(field.getContent());

                }
            }
            outputStream.write(new byte[]{FOOTER_1, FOOTER_2});

            return outputStream.toByteArray();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }


    public static MainPacket parse(byte[] data) {
        if (data[0] != HEADER_1 && data[1] != HEADER_2 || data[data.length - 1] != FOOTER_2 && data[data.length - 2] != FOOTER_1) {
            throw new IllegalArgumentException("Unknown packet format");

        }
        byte type = data[2];
        byte subType = data[3];
        MainPacket packet = MainPacket.create(type, subType);
        int offset = 4;
        while (true) {
            if (data.length - 2 <= offset) {
                return packet;
            }

            byte fieldId = data[offset];
            byte fieldSize = data[offset + 1];

            byte[] content = new byte[fieldSize];

            if (fieldSize != 0) {

                System.arraycopy(data, offset + 2, content, 0, fieldSize);

            }

            MainField field = new MainField(fieldId, fieldSize, content);

            packet.getFields().add(field);

            offset += 2 + fieldSize;
        }

    }

    public MainField getField(int id) {
        Optional<MainField> field = getFields().stream()
                .filter(f -> f.getId() == (byte) id)
                .findFirst();

        if (field.isEmpty()) {
            throw new IllegalArgumentException("No field with that id");
        }
        return field.get();
    }

    public String getValue(int id) {
        MainPacket.MainField field = getField(id);

        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(field.getContent());
             ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream)) {

            return (String) objectInputStream.readObject();

        } catch (IOException | ClassNotFoundException e) {
            throw new IllegalArgumentException(e);
        }


    }
}
