package oldprotocol;

import java.io.*;

public class HandShakeTypeHandler {//1.1

    private MainPacket packet;


    public HandShakeTypeHandler(MainPacket packet) {
        this.packet = packet;
    }

    public String checkPing() {

        MainPacket.MainField field = packet.getFields().get(0);
        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(field.getContent());
             ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream)) {

            String value = (String) objectInputStream.readObject();

            if (value.equals("ping")) {
                addPong();
                return value;
            } else {
                throw new IllegalArgumentException("Это не наш протокол");
            }


        } catch (IOException | ClassNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }


    public String checkPong() {

        MainPacket.MainField field = packet.getFields().get(0);
        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(field.getContent());
             ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream)) {

            String value = (String) objectInputStream.readObject();

            if (value.equals("pong")) {
                return value;

            } else {
                throw new IllegalArgumentException("Это не наш протокол");
            }

        } catch (IOException | ClassNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private void addPong() {
        MainPacket.MainField field;
        field = new MainPacket.MainField((byte) 1);

        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream)) {

            objectOutputStream.writeObject("pong");
            byte[] data = byteArrayOutputStream.toByteArray();

            field.setSize((byte) data.length);
            field.setContent(data);


        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }


        packet.getFields().set(0, field);
    }

    public void addPing() {



        MainPacket.MainField field = new MainPacket.MainField((byte) 1);

        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream)) {

            objectOutputStream.writeObject("ping");
            byte[] data = byteArrayOutputStream.toByteArray();

            field.setSize((byte) data.length);
            field.setContent(data);


        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        packet.getFields().add(field);

    }

}
