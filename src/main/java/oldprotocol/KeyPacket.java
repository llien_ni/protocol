package oldprotocol;


import lombok.Data;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

@Data
public class KeyPacket {

    private static final byte HEADER_1 = (byte) 0xe4;
    private static final byte HEADER_2 = (byte) 0x15;


    private static final byte FOOTER_1 = (byte) 0x02;
    private static final byte FOOTER_2 = (byte) 0x71;

    private byte keySize;
    private byte[] key;
    private byte saltSize;
    private byte[] salt;



    public static KeyPacket create(String keyValue, String salt) {


        KeyPacket packet = new KeyPacket();

        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream)) {


            objectOutputStream.writeObject(keyValue);
            byte[] data = byteArrayOutputStream.toByteArray();


            objectOutputStream.writeObject(salt);
            byte[] dataSalt = byteArrayOutputStream.toByteArray();


            packet.keySize = (byte) data.length;
            packet.key = data;

            packet.saltSize = (byte) dataSalt.length;
            packet.salt = dataSalt;

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        return packet;

    }

    public static SecretKey getKeyFromPassword(String password, String salt)
            throws NoSuchAlgorithmException, InvalidKeySpecException {

        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
        KeySpec spec = new PBEKeySpec(password.toCharArray(), salt.getBytes(), 2, (byte)10);
        SecretKey secret = new SecretKeySpec(factory.generateSecret(spec)
                .getEncoded(), "AES");
        return secret;
    }

    public byte[] toByteArray() {
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {

            outputStream.write(new byte[]{HEADER_1, HEADER_2});


            outputStream.write(keySize);
            outputStream.write(key);
            outputStream.write(saltSize);
            outputStream.write(salt);

            outputStream.write(new byte[]{FOOTER_1, FOOTER_2});

            return outputStream.toByteArray();

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }


    public static KeyPacket parse(byte[] data) {
        if (data[0] != HEADER_1 && data[1] != HEADER_2 || data[data.length - 1] != FOOTER_2 && data[data.length - 2] != FOOTER_1) {
            throw new IllegalArgumentException("Unknown packet format");

        }

        KeyPacket keyPacket = new KeyPacket();

        byte keySize = data[2];

        byte[] key = new byte[keySize];
        if(keySize != 0){
            System.arraycopy(data, 3, key, 0, keySize);
        }
        byte saltSize = data[2+keySize+1];
        byte[] salt = new byte[saltSize];
        if(saltSize != 0){
            System.arraycopy(data, 2+keySize+1, salt, 0, saltSize);
        }
        keyPacket.setKey(key);
        keyPacket.setSalt(salt);
        return keyPacket;

    }

    public String getValue() {


        try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(key);
             ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream)) {

            return (String) objectInputStream.readObject();

        } catch (IOException | ClassNotFoundException e) {
            throw new IllegalArgumentException(e);
        }


    }

}
