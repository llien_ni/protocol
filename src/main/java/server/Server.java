package server;


import listeners.MessageListener;
import lombok.Data;
import oldprotocol.MainPacket;
import protocol.MessagePacket;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


@Data
public class Server {


    protected ServerSocket serverSocket;
    protected final int port;

    protected List<MessageListener> listeners;
    protected List<Socket> sockets;
    protected List<SocketClient> clients;


    public Server(int port){
        this.port = port;
        initServer();
    }

    public void initServer() {

        try{
            this.serverSocket = new ServerSocket(port);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        listeners = new ArrayList<>();
        sockets = new ArrayList<>();

        clients = new ArrayList<>();


    }

    public void registerListener(MessageListener messageListener){
        messageListener.init(this);
        listeners.add(messageListener);
    }

    public void start(){

        try {
            while(true){
                Socket socket = serverSocket.accept();
                processingSocket(socket);
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    private void processingSocket(Socket socket){

        sockets.add(socket);
        int id = sockets.size();

        SocketClient client = SocketClient.create(this, socket, clients.size() + 1);
        clients.add(client);

        System.out.println("Клиент постучался - " + client.getId());
        System.out.println(clients.size());

        new Thread(client).start();

    }


    public void sendMessage(MessagePacket messagePacket, SocketClient socketClient){

        try {
            System.out.println("клиенту с id-" + socketClient.id + " отправили сообщение с типом " + messagePacket.getType());
            socketClient.outputStream.write(messagePacket.toByteArrayMy());
            socketClient.outputStream.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public void acceptMessage(MessagePacket messagePacket, SocketClient sender){

        System.out.println("check type");

        System.out.println(messagePacket.getType());
        System.out.println(messagePacket.getSubType());
        System.out.println(listeners.size());
        for(MessageListener listener: listeners){

            System.out.println("listener "+listener.getTypeOFMessage() +" "+ listener.getSubTypeOfMessage());
            if(messagePacket.getType() == listener.getTypeOFMessage()&& messagePacket.getSubType() == listener.getSubTypeOfMessage()){
                System.out.println("type found");
                listener.handleMessage(messagePacket, sender);
            }
        }

    }

    private byte[] extendArray(byte[] oldArray) {
        int oldSize = oldArray.length;
        byte[] newArray = new byte[oldSize * 2];
        System.arraycopy(oldArray, 0, newArray, 0, oldSize);
        return newArray;
    }




//    public void start(int port){
//        try{
//            server = new ServerSocket(port);
//            System.out.println("СЕРВЕР ЗАПУЩЕН");
//
//            Socket clientSocket = server.accept();
//            System.out.println("КЛИЕНТ ПОДКЛЮЧЕН");
//
//            initializeClientStreams(clientSocket);
//
//            runMessaging();
//
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private void initializeClientStreams(Socket socket){
//        try {
//            this.inputStream = socket.getInputStream();
//            this.outputStream  = socket.getOutputStream();
////            this.fromClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
////            this.toClient = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void runMessaging(){
//        Scanner scanner = new Scanner(System.in);
//
//        while(true){
////            try{
//
//
//                //общаемся с помощью нашего пакета
//
////                byte[] data = readInput(inputStream);
////                MainPacket packet = MainPacket.parse(data);
////
////                if(packet.getType() == 2){
////
////                    StandardPacket packet1 = StandardPacket.create(2);
////                    packet1.setValue(1, "end");
////
////
////                    outputStream.write(packet1.toByteArray());
////                    break;
////                }
////
////                String value1 = packet.getValue(1);
////                String value2 = packet.getValue(2);
////                System.out.println("ПОЛУЧИЛИ ОТ КЛИЕНТА: "+ value1);
////                System.out.println("ПОЛУЧИЛИ ОТ КЛИЕНТА: "+ value2);
////
////                StandardPacket response = StandardPacket.create(1);
////                response.setValue(1, "Thanks for information");
////
////                outputStream.write(response.toByteArray());
////                outputStream.flush();
////
////            } catch (IOException e) {
////                throw new IllegalArgumentException(e);
////            }
//        }
//    }
//
//    private byte[] readInput(InputStream stream) throws IOException{
//        int b;
//        byte[] buffer = new byte[10];
//        int counter = 0;
//
//        while((b = stream.read())> -1){
//            buffer[counter++] =(byte) b;
//
//            if(counter >= buffer.length){
//                buffer = extendArray(buffer);
//
//            }
//            //проверяет конец пакета
//            if (counter > 1 && MainPacket.compareEndOfPacket(buffer, counter - 1)){
//
//                break;
//            }
//        }
//        byte[] data = new byte[counter];
//        System.arraycopy(buffer, 0, data, 0, counter);
//
//        return data;
//    }
//
//    private byte[] extendArray(byte[] oldArray){
//        int oldSize = oldArray.length;
//        byte[] newArray = new byte[oldSize *2];
//        System.arraycopy(oldArray, 0, newArray, 0, oldSize);
//        return  newArray;
//
//    }
//


}
