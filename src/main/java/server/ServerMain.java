package server;

import listeners.GoodByeTypeListener;
import listeners.HandShakeTypeListener;
import listeners.StandardNotSecureTypeListener;
import oldprotocol.KeyPacket;

import java.io.IOException;

public class ServerMain {
    public static void main(String[] args) {

        Server server = new Server(4444);

        server.registerListener(new HandShakeTypeListener());
        server.registerListener(new GoodByeTypeListener());
        server.registerListener(new StandardNotSecureTypeListener());
        server.start();

    }
}
