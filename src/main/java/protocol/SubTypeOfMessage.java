package protocol;

public enum SubTypeOfMessage {


    GOODBYE((byte) 1),
    HANDSHAKE((byte) 2),
    STANDARD_NOT_SECURED((byte) 1),
    STANDARD_SECURED((byte) 2);


    private final byte subType;

    SubTypeOfMessage(byte subType) {
        this.subType = subType;
    }

    public byte getSubType(){return subType;}



//    SubTypeOfMessage(byte b) {
//
//    }
}
