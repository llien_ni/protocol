package protocol;

public enum TypeOfMessage {

    STANDARD((byte) 2),
    META((byte) 1);




    private final byte type;

    TypeOfMessage(byte type) {
        this.type = type;
    }

    public byte getType(){return type;}

    public static SubTypeOfMessage findTypeBy(byte type, byte subType){
        TypeOfMessage typesOfMessages = null;
        for(TypeOfMessage types : values()){
            if(types.type == type){
                typesOfMessages = types;
                break;
            }
        }
        System.out.println("Type "+typesOfMessages);
        SubTypeOfMessage[] subTypeOfMessages = new SubTypeOfMessage[2];
        if (typesOfMessages.getType() == (byte) 1){

            subTypeOfMessages[0]=SubTypeOfMessage.GOODBYE;
            subTypeOfMessages[1]=SubTypeOfMessage.HANDSHAKE;
        }
        if(typesOfMessages.getType() == (byte) 2){

            subTypeOfMessages[0]=SubTypeOfMessage.STANDARD_NOT_SECURED;
            subTypeOfMessages[1]=SubTypeOfMessage.STANDARD_SECURED;}


        SubTypeOfMessage subTypeOfMessage = null;
        for(SubTypeOfMessage types: subTypeOfMessages){
            if(types.getSubType() == subType){
                subTypeOfMessage = types;
                break;
            }
        }
        System.out.println("Subtype " +subType);
        return subTypeOfMessage;
    }




}
