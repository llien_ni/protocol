package protocol;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Data
public class MessagePacket {

    private static final byte HEADER_1 = (byte) 0xe4;
    private static final byte HEADER_2 = (byte) 0x15;

    private static final byte FOOTER_1 = (byte) 0x00;
    private static final byte FOOTER_2 = (byte) 0x90;

    byte type;
    byte subType;

    private List<Field> fields = new ArrayList<>();

    private MessagePacket() {}


    public static MessagePacket create(byte type, byte subType){
        MessagePacket packet = new MessagePacket();

        System.out.println("Это тип который пришел "+ type);
        System.out.println("Это подтип который пришел "+ subType);
        // проверка, что корректный тип пакета
        //еще нужно добавить проверку на подтип пакета
        if(TypeOfMessage.findTypeBy(type, subType) != null){
            packet.setType(type);
            packet.setSubType(subType);
            System.out.println(type);
            System.out.println(subType);
        } else{
            throw new IllegalArgumentException("could not define type");
        }

        return packet;
    }

    public byte[] toByteArrayMy(){


        try(ByteArrayOutputStream writer = new ByteArrayOutputStream()){

            writer.write(new byte[] {HEADER_1, HEADER_2, type, subType});

            for(Field field: fields){
                writer.write(new byte[]{field.getId(), field.getSize()});
                writer.write(field.getContent());
            }

            writer.write(new byte[]{FOOTER_1, FOOTER_2});

            return writer.toByteArray();
        } catch (IOException e){
            throw new IllegalArgumentException(e);
        }
    }

    public static MessagePacket parse(byte[] data){


        if(data[0] != HEADER_1 && data[1] != HEADER_2
                || data[data.length -1] != FOOTER_2 && data[data.length -2] != FOOTER_1){
            throw new IllegalArgumentException("this is another packet");
        }

        byte type = data[2];
        byte subType = data[3];

        MessagePacket messagePacket = MessagePacket.create(type, subType);
        //замени на 4 потому что тут не только типы, но и подтипы есть
        int offset = 4;
        while(true){
            if(data.length -2 <= offset){
                return messagePacket;
            }

            byte fieldId = data[offset];
            byte fieldSize = data[offset + 1];

            byte[] content = new byte[Byte.toUnsignedInt(fieldSize)];

            if(fieldSize != 0){
                System.arraycopy(data, offset + 2, content, 0, Byte.toUnsignedInt(fieldSize));
            }

            Field field = new Field(fieldId, fieldSize, content);
            messagePacket.getFields().add(field);

            offset += 2 + fieldSize;

        }
    }

    public static boolean compareEndOfPacket(byte[] arr, int lastItem){
        return arr[lastItem - 1] == FOOTER_1 && arr[lastItem] == FOOTER_2;
    }


    public Field getField(int id){

        Optional<Field> field = getFields().stream().filter(f -> f.getId() == (byte) id).findFirst();

        if(field.isEmpty()){
            throw new IllegalArgumentException("No field with that id");
        }
        return field.get();
    }

    public void setContentInField(int id, Object value){
        Field field;

        try {
            field = getField((byte) id);
        } catch (IllegalArgumentException e){
            field = new Field((byte) id);
        }

        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutputStream oos = new ObjectOutputStream(bos)){

            oos.writeObject(value);

            byte[] data = bos.toByteArray();

            field.setSize((byte) data.length);
            field.setContent(data);

        } catch (IOException e){
            throw new IllegalArgumentException(e);
        }

        getFields().add(field);
    }

    public <T> T getContentFromField(int id, Class<T> tClass){

        Field field = getField(id);


        try(ByteArrayInputStream bis = new ByteArrayInputStream(field.getContent());
            ObjectInputStream ois = new ObjectInputStream(bis)){

            return (T) ois.readObject();
        } catch (IOException | ClassNotFoundException e){
            throw new RuntimeException(e);
        }
    }




    @Data
    @AllArgsConstructor
    public static class Field{

        private byte id;
        private byte size;
        byte[] content = new byte[size];

        public Field(byte id){this.id = id;}

    }
}
